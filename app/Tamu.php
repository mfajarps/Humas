<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tamu extends Model
{
    protected $fillable = ['nama_tamu','notlp','jenis_tamu','exit_at','keperluan'];
}
