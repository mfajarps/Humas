<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tamu;


class TamuController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }
    public function index(){
      $tamu_hari_ini = Tamu::whereRaw('DATE(created_at) = CURDATE()')->count();
      $tamu_keseluruhan = Tamu::count();
      $tamu_orangtua = Tamu::where('jenis_tamu','orangtua')->count();
      $tamu_alumni = Tamu::where('jenis_tamu','alumni')->count();
      $tamu_tamu = Tamu::where('jenis_tamu','tamu')->count();

      return view('tamu.dashboard',
      compact(
        'tamu_hari_ini',
        'tamu_keseluruhan',
        'tamu_orangtua',
        'tamu_alumni',
        'tamu_tamu')
      );
    }

    public function show_input(){
      $jenis = ['orangtua','tamu','alumni'];
      return view('tamu.Input_Tamu', compact('jenis'));
    }

    public function hapus($id){
      Tamu::destroy($id);
      return back()->withMessage('Berhasil');
    }

    public function simpan(Request $request){
      // $this->rules($request);
      Tamu::create($request->all());
      return back()->withMessage('Berhasil');
    }

    public function data(){
      $data = Tamu::whereRaw('DATE(created_at) = CURDATE() AND exit_at IS NULL')->get();
      return view('tamu.Data_Tamu', compact('data'));
    }

    public function edit($id){
      $jenis = ['orangtua','tamu','alumni'];
      $data = Tamu::where('id',$id)->first();
      return view('tamu.Edit_Tamu', compact('data','jenis'));
    }

    public function update(Request $request, $id){
      Tamu::where('id',$id)->update([
        'nama_tamu'=>$request->nama_tamu,
        'notlp'=>$request->notlp,
        'keperluan'=>$request->keperluan,
        'jenis_tamu'=>$request->jenis_tamu,
      ]);
      return redirect('tamu/data')->withMessage('Data Berhasil Diubah');
    }

    public function pulang($id){
      $data = date("Y-m-d H:i:s");
      Tamu::where('id',$id)->update([
        'exit_at'=>$data,
      ]);
      return back()->withMessage('Tamu Telah Pergi');
    }

    public function laporan($ket){
      if($ket == "today"){
        $data = Tamu::whereRaw('DATE(created_at) = CURDATE() AND exit_at is NULL')->get();
      }else if ($ket == "all"){
        $data = Tamu::all();
      }else if($ket == "stay")
      {
        $data = Tamu::whereRaw('DATE(created_at)  = CURDATE() AND exit_at is  NULL')->get();
      }else if($ket == "leave"){
        $data = Tamu::whereRaw('DATE(created_at)  = CURDATE() AND exit_at is NOT NULL')->get();
      }
      $laporan = $ket;
      return view ('tamu.laporan',compact('laporan','data'));
    }

}
