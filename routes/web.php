<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'tamu'], function(){
Route::get('/','TamuController@index');
Route::get('/input','TamuController@show_input');
Route::get('/hapus/{id}','TamuController@hapus');
Route::post('/simpan','TamuController@simpan');
Route::get('/data','TamuController@data');
Route::get('/pulang/{id}','TamuController@pulang');
Route::get('/edit/{id}','TamuController@edit');
Route::post('/update/{id}','TamuController@update');
Route::get('/laporan/{ket}','TamuController@laporan');
});
