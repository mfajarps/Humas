@extends('layouts.template')

@section('title.Dashboard')

@section('content')

<div class="container">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Form Input Tamu</h4>
          <p class="card-subtitle">Isi Data Dengan Benar</p><hr>

          <form action="{{ url('tamu/simpan') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="">Nama Lengkap</label>
              <input class="form-control" type="text" name="nama_tamu" value="">
            </div>

            <div class="form-group">
              <label for="">Jenis Tamu</label>
              <select class="form-control" name="jenis_tamu">
                <option value=""></option>
                @foreach($jenis as $field)
                <option value="{{ $field }}">{{ $field }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="">No Telepon</label>
              <input class="form-control" type="number" name="notlp" value="">
            </div>

            <div class="form-group">
              <label for="">Keperluan</label>
              <textarea class="form-control" name="keperluan" rows="8" cols="80"></textarea>
            </div>
            <button class="btn btn-info" type="submit" name="simpan">SIMPAN</button>
          </form>

        </div>
      </div>
    </div>
  </div>

</div>

@endsection
