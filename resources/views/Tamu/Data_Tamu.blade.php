@extends('layouts.template')

@section('title.Dashboard')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data Tamu</h4>

          <table class="table table-bordered table-striped table-hover" id="example23">
            <thead class="text-center">
              <tr>
                <th>No</th>
                <th>Nama lengkap</th>
                <th>Jenis Tamu</th>
                <th>No Telepon</th>
                <th>Keperluan</th>
                <th>Tanggal Masuk</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody class="text-center">
              @foreach($data as $field)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $field->nama_tamu }}</td>
                <td>{{ $field->jenis_tamu }}</td>
                <td>{{ $field->notlp }}</td>
                <td>{{ $field->keperluan }}</td>
                <td>{{ $field->created_at->diffForHumans() }}<br>
                    {{ $field->created_at }}
                </td>
                <td>
                  <a href="{{ url('tamu/pulang/'.$field->id) }}" class="btn btn-danger btnPulang"><i class="fa fa-sign-out"></i></a>
                  <a href="{{ url('tamu/edit/'.$field->id) }}" class="btn btn-danger btnEdit"><i class="fa fa-pencil"></i></a>
                  <a href="{{ url('tamu/hapus/'.$field->id) }}" class="btn btn-danger btnHapus"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <script>
                $(document).ready(function(){
                  $('.btnHapus').on('click', function(e){
                    e.preventDefault();
                    var href = $(this).attr('href');
                  swal({
                    title : "Apakah Anda Yakin ?",
                    text : "Data Akan Dihapus",
                    icon : "warning",
                    buttons : true,
                    dangerMode : true,
                  })
                  .then(willDelete)=>{
                    if(willDelete){
                      window.location.href = href;
                    }
                  });
                });

                $('.btnPulang').on('click', function(e){
                  e.preventDefault();
                  var href = $(this).attr('href');
                swal({
                  title : "Apakah Anda Yakin ?",
                  text : "Kamu akan pergi ;) ?",
                  icon : "info",
                  buttons : true,
                  dangerMode : true,
                })
                .then(willDelete)=>{
                  if(willDelete){
                    window.location.href = href;
                  }
                });
              });

            });
              </script>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
