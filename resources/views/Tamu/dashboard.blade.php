@extends('layouts.template')

@section('title','Dashboard')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Dashboard</h4>
            <p class="card-subtitle">Jumlah Tamu</p>
            <div class="row">
              <div class="col-sm-6">
                <div class="card card-dark">
                  <div class="card-body">
                    <h2 class="font-light text-white text-center">{{ $tamu_hari_ini }}</h2>
                    <hr style="background-color:white;">
                    <p class="text-center text-white">TAMU HARI INI</p>

                  </div>

                </div>

              </div>
              <div class="col-sm-6">
                <div class="card card-dark">
                  <div class="card-body">
                    <h2 class="font-light text-white text-center">{{ $tamu_keseluruhan}}</h2>
                    <hr style="background-color:white;">
                    <p class="text-center text-white">TAMU KESELURUHAN</p>

                  </div>

                </div>

              </div>

            </div>
            <!-- <h4 class="card-title">Jenis Tamu</h4> -->
              <p class="card-subtitle">Jenis Tamu</p>
              <hr>
            <div class="row">



            <div class="col-sm-4">
              <div class="card card-dark">
                <div class="card-body">
                  <h1 class="font light text-white text-center">{{ $tamu_orangtua }}</h1>
                  <hr style="background-color:white;">
                  <p class="text-center text-white">Orang tua</p>
                </div>

              </div>

            </div>
            <div class="col-sm-4">
              <div class="card card-dark">
                <div class="card-body">
                  <h1 class="font light text-white text-center">{{ $tamu_alumni}}</h1>
                  <hr style="background-color:white;">
                  <p class="text-center text-white">Alumni</p>
                </div>

              </div>

            </div>
            <div class="col-sm-4">
              <div class="card card-dark">
                <div class="card-body">
                  <h1 class="font light text-white text-center">{{ $tamu_tamu}}</h1>
                  <hr style="background-color:white;">
                  <p class="text-center text-white">Reguler</p>
                </div>

              </div>


            </div>
            </div>
        </div>

      </div>

    </div>


  </div>

</div>

@endsection
