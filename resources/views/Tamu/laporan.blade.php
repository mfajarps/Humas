@extends ('layouts.template')

@section('title','Data tamu')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Data {{ $laporan }}</h4>
          <!-- <p class="card-subtitle">Data {{ $laporan}}</p> -->
          <p class="card-subtitle">Tanggal : {{ date("Y-m-d")}}</p>
          <hr>
          <table class="table table-bordered table-striped table-hover " id="example23">
            <thead>
              <tr>
                <td>No</td>
                <td>Nama Lengkap</td>
                <td>Jenis Tamu</td>
                <td>No Telepon</td>
                <td>Keperluan</td>
                <td>Tanggal Masuk</td>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $field)
              <tr>
                <td>{{  $loop->index + 1}}</td>
                <td>{{ $field->nama_tamu}}</td>
                <td>{{ $field->jenis_tamu}}</td>
                <td>{{ $field->notlp}}</td>
                <td>{{ $field->keperluan}}</td>
                <td>{{ $field->created_at }}</td>

              </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>

    </div>

  </div>

</div>

@endsection
